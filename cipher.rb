class Cipher
  class CipherValidationError < StandardError; end
  
  def self.encode(string, distance: 1)
    lowercase_string = string.downcase
    raise CipherValidationError, 'Only lowercase alphabets are expected.' unless lowercase_string.scan(/[^a-z]/).empty?
    ascii_codes = lowercase_string.chars.map(&:ord).map { |code| code + distance }

    ascii_codes.map do |code|
      remainder = code % 123
      if code > 122 
        97 + (remainder - 26 * (remainder / 26))
      else
        remainder
      end
    end.map(&:chr).join
  end

  def self.decode(string, distance: 1)
    lowercase_string = string.downcase
    raise CipherValidationError, 'Only lowercase alphabets are expected.' unless lowercase_string.scan(/[^a-z]/).empty?
    ascii_codes = lowercase_string.chars.map(&:ord).map { |code| code - distance }
    ascii_codes.map { |code| code < 97 ? code + 26 * ((122 - code) / 26) : code }.map(&:chr).join
  end
end
